# CHANGELOG

## Unreleased

### Added

- postgres/postgres-dev: allow to pass an arbitrary migration function (and config). The `migratus` key is supported for
  backward compatibility.

- bump dependencies

## 0.5.58 (2022-06-12)

### Fixed

- allows dynamic router with a non var value, albeit with a force flag to make the user understand
  the caveats: the function must have a var resolution of a route function inside, so that function
  redefinition really is used to compute the routes on the next call.

## 0.5.54 (2022-06-12)

### Added

- value basic block to switch between constants, envvars and file content between
  environments.

## 0.5.50 (2022-06-12)

### Changed

- dependency updates

## 0.5.46 (2022-05-29)

### Fixed

- The halt function for many components

### Added

- `cb/resolve-block` to resolve a block from a system with the block-key; usefull for tests and repl
- block-dev project for development (delegates most to integrant-repl)

### Changed

- dependency updates, in particult xtdb in version 1.21.0

## 0.5.34 (2022-05-07)

**Breaking Change**: the construction is different.

### Fixed

- fix blocks-rabbitmq dependency for blocks-rabbitmq-dev

## 0.4.14 (2022-04-21)

### Added

- blocks for rabbitmq

## 0.4.10 (2022-04-18)

### Changes

- new repository with all libraries together.

### Fixes

- aero config with file will now fails if file does not exists.

## 0.3.28

### Changed

- Tooling

## 0.3.22

### Changed

- new anvil tooling version

## 0.3.20

### Changed

- using anvil tooling for building and deployment

## 0.2.12 (2022-01-02 / 4b31c52)

### Added

- block transform multimethods for [integrant](https://github.com/weavejester/integrant).
- config blocks with [aero](https://github.com/juxt/aero).
- awesome [tooling](https://github.com/lambdaisland/open-source) by [lambdaisland](https://lambdaisland.com).