(ns libs
  (:require [babashka.fs :as fs]
            [lint]
            [outdated]
            [test]))

(def lib-dirs (fs/list-dir "libraries"))

(defn test-libs []
  (doseq [directory libs/lib-dirs]
    (test/run-tests directory)))

(defn lint-libs []
  (doseq [directory libs/lib-dirs]
    (lint/lint directory)))

(defn check-outdated-deps []
  (doseq [directory (concat ["." "nvd"
                             (outdated/tmp-dir-for-deps-map #'outdated/deps)
                             (outdated/tmp-dir-for-deps-map #'test/deps)
                             (outdated/tmp-dir-for-deps-map #'lint/deps)]
                            libs/lib-dirs)]
    (outdated/check-outdated-deps directory *command-line-args*)))